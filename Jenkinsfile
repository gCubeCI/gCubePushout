#!groovy

/**
 * Checkouts and pushes gCube software metadata to the software-version service
 *
 * Roberto Cirillo (ISTI-CNR)
 */
def agent_root_folder = '/var/lib/jenkins/.m2'
println "Type of build: ${params.Type}"

//locate the build file
//String reportURL = "https://code-repo.d4science.org/gCubeCI/gCubeReleaseConfigs/raw/branch/master/closed/${gCube_release_version}/build_commits.${report_number}.csv"
//just for test a fake build file
String reportURL="https://code-repo.d4science.org/TestActions/release_test/raw/branch/master/build_commits.1375.csv"
println "Querying ${reportURL}"

//load and parse the release file
def text = reportURL.toURL().getText()
def components = parseBuildCommits(text)
assert 0 < components.size(): "No component found in build_commits.${report_number}.csv"
for (component in components) {
    println "$component"
}


pipeline {

    agent {
        label 'CD'
    }

    environment {
        AGENT_ROOT_FOLDER = "${agent_root_folder}"
        GCUBE_RELEASE_NUMBER = "${params.gCube_release_version}"
        PIPELINE_BUILD_NUMBER = "${env.BUILD_NUMBER}"
        TYPE = "${params.Type}"
        PUSH_REPORT = "${agent_root_folder}/pushes.${report_number}.csv"
    }

    parameters {

        choice(name: 'Type',
                choices: ['DRY-RUN', 'PUSH'],
                description: 'The type of execution')

        string(name: 'gCube_release_version',
                defaultValue: 'x.y.z',
                description: 'The number of the gCube release. Sample values: 4.14, 4.15, etc.')
        string(name: 'report_number',
                defaultValue: '',
                description: 'The report number to use for push operation.')

    }

    stages {
        stage('initialize report') {
            steps {
                sh '''
                   date=`date`
                   echo "#Push ${PIPELINE_BUILD_NUMBER},,,,," > $PUSH_REPORT
                   echo "#StartTime ${date},,,,," >> $PUSH_REPORT
                   echo "#Release ${gCube_release_version},,,,," >> $PUSH_REPORT
                   echo "ArtifactID,Version,SCM URL,Build Number,Push response" >> $PUSH_REPORT
                   '''
            }
        }
        stage('clone and push') {
            steps {
                script {
                    for (int i = 0; i < components.size(); i++) {
                        stage(components[i]['name']) {
                            echo "About to push ${components[i]['name']} with version ${components[i]['version']}"
                            checkout_from_reference(components[i]['name'], components[i]['gitRepo'],
                                        components[i]['commitID'], components[i]['version'], gCube_release_version)
       //                     sh "echo -e  ${components[i]['name']},${components[i]['version']},${components[i]['gitRepo']},${components[i]['commitID']},v${components[i]['version']},r${gCube_release_version} >> $PUSH_REPORT"
                        }
                    }
                }
            }
        }
    }

    // post-build actions
    post {
        always {
            script {
                sh '''
                  cp $PUSH_REPORT ./push_jobs.${PIPELINE_BUILD_NUMBER}.csv
                  cat ./push_jobs.${PIPELINE_BUILD_NUMBER}.csv
                  '''
            }
        }
        success {
            echo 'The gCubePushingOut pipeline worked!'
            emailext to: 'roberto.cirillo@isti.cnr.it',
                    subject: "[Jenkins PUSH D4S] ${TYPE} push ${currentBuild.fullDisplayName} worked",
                    body: "Build time: ${currentBuild.durationString}. See ${env.BUILD_URL}"
            emailext attachmentsPattern: "**/push_jobs.${PIPELINE_BUILD_NUMBER}.csv",
                    to: 'roberto.cirillo@isti.cnr.it',
                    subject: "${TYPE} push report for ${GCUBE_RELEASE_NUMBER} (build #${PIPELINE_BUILD_NUMBER})",
                    body: "${currentBuild.fullDisplayName}. Build time: ${currentBuild.durationString}. See ${env.BUILD_URL}"
        }
        failure {
            echo 'The gCubePushingOut pipeline has failed'
            emailext attachLog: true,
                    to: 'roberto.cirillo@isti.cnr.it',
                    subject: "[Jenkins PUSH D4S] push ${currentBuild.fullDisplayName} failed",
                    body: "Something is wrong with ${env.BUILD_URL}"
        }
    }
}


/**
 * Clones the repository and push related metadata
 * NOTE: 'credentialsId' be manually configured in Jenkins to access all the repos
 */
def checkout_from_reference(repo_name, repo_url, commit, version, gCube_release_version) {
    //def repo_url = "git@bit   bucket.org:USER/${reponame}.git"

    echo "Checkout SHA from reference $commit"
    sh(script: "rm -r ${repo_name} || true", returnStdout: true)?.trim()
    checkout([
            $class                           : 'GitSCM',
            branches                         : [[name: '*/master']],
            doGenerateSubmoduleConfigurations: false,
            extensions                       : [
                    [$class: 'RelativeTargetDirectory', relativeTargetDir: repo_name],
                    [$class: 'CloneOption', noTags: false, reference: '']
            ],
            submoduleCfg                     : [],
            userRemoteConfigs                : [
                    [credentialsId: '88b54962-1c0e-49cb-8155-22276860f346', url: repo_url] //git.gcube credentials on jenkins
            ]
    ])
    if (params.Type == "PUSH") {
        def status=push(repo_url, repo_name, "v${version}", "r${gCube_release_version}", commit)
        sh "echo -e  ${repo_name},${version},${repo_url},${commit},${status} >> $PUSH_REPORT"
    }else{
        sh "echo -e  ${repo_name},${version},${repo_url},${commit},'unsent' >> $PUSH_REPORT"
    }

}

/**
   Push the metadata to the software versioning service (TO-DO)
 */
def push(repo_url, repo_name, tag, gCube_release_version, commit) {
    def fileContent
    dir(repo_name) {
        withCredentials([usernamePassword(credentialsId: '88b54962-1c0e-49cb-8155-22276860f346', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
            def repository = repo_url.replaceFirst(".+://", "https://${GIT_USERNAME}:${GIT_PASSWORD}@")
            sh("""
            git remote set-url origin $repository
            git fetch --all
            mvn clean generate-resources
            """)
        }
        fileContent = readJSON(file: "target/classes/META-INF/export.json", encoding: "UTF-8")
        println(fileContent);
    }
 	return "200fake"
 //   postCall(fileContent)

}

/**
    service-versioning api rest call
*/
def postCall(fileContent){
    // POST
    def post = new URL("https://servicehost/software").openConnection();
    post.setRequestMethod("POST")
    post.setDoOutput(true)
    post.setRequestProperty("Content-Type", "application/json")
    post.getOutputStream().write(fileContent.getBytes("UTF-8"));
    def postRC = post.getResponseCode();
    println(postRC);
    if(postRC.equals(200)) {
        println(post.getInputStream().getText());
    }
}


//a non CPS method is necessary for the usage of splitEachLine()
@NonCPS
def parseBuildCommits(def text) {
    def components = []
    "${text}".splitEachLine(',') { columns ->
        if (columns[0].startsWith('#') || columns[0].startsWith('GroupID'))
            return
        components.add([
                name    : columns[1],
                version : columns[2],
                gitRepo : columns[3],
              // is it useful?
                commitID: columns[4],
                distributionURL: columns[5],
              // is it useful?
                filename: columns[6],
                packaging: columns[7]
         ]
        )
    }
    return components
}